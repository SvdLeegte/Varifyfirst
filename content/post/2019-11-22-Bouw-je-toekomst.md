---
Title: Bouw je toekomst 
subtitle: Eten is namelijk essentieel en daarvoor heb je een goede keuken nodig
date: 2019-11-22
tags: ["bruynzeelkeukens", "gebruikersonderzoek", "usabilitytest", "keukenconfigurator",
"drama", "drieparticipanten", "verschillendeinzichten"]
---
### Deze week heb ik 40,75 uur gewerkt
Ik merk dat ik gebruikersonderzoek d.m.v. usertests echt heel erg leuk vind
om te doen! De vorige keer heb ik de mogelijkheid gehad om zelf het interview
af te nemen. Dit keer was het echt voor een betaalde opdrachtgever. Toch heeft
Claire mij de mogelijkheid gegeven om een van de gebruikersinterviews af te 
nemen. Ik merkte dat ik het heel erg interessant vind om een test te observeren. 
Plus dat ik bang was om het verkeerd te doen terwijl de opdrachtgever ook aan
het observeren was. Als ik me er iets meer op voor kan bereiden (nu kwam het
vrij onverwachts) dan zou ik het wel erg graag willen doen. Toch denk ik dat ik
eerst nog wat meer wil oefenen omdat ik de vorige keer merkte dat ik heeeel 
veel aan het nadenken ben om niet teveel in te vullen voor de gebruiker. 
De testopstelling was niet heel erg ideaal. Idealiter hadden we nog een extra
scherm waar ik de andere kamer op kon streamen. Nu stond dit namelijk op mijn
kleine 13 inch laptop waar ik ook nog de aantekenigen op moest maken! 
5 paar ogen op mijn kleine scherm en ook op de aantekeningen. Wat ik best een 
risico vind, want ook al nemen we het hele gesprek op zodat ik het later nog 
eens terug kan luisteren en quotes kan noteren, kunnen mijn notities toch van
invloed zijn op de opdrachtgever (hoe betrouwbaar mijn aantekeningen zijn t.o.v.
wat zij  belangrijk achten bijvoorbeeld). 
Maaaaaar, het ging goed en de opdrachtgever was erg enthousiast! 
<br>
<br>
Tijd om alles te verwerken in een analyse en rapportage. Samen met Claire hebben
we dezelfde middag nog alle resultaten weten te analyseren en noteren in een 
excel sheet. Claire heeft een opzet gedaan voor een officiele rapportage. 
Hierin heb ik kunnen ondersteunen en input kunnen geven als aanvulling op het
rapport en advies. Verder was het voor mij vooral een leerschool hoe een 
officiele rapportage er nu precies uitziet. 
<br>
<br>
Mooi om te zien is hoe onbewust je gebruiker is van zijn of haar input. Ik had
dit al gemerkt tijdens de gebruikerstest van Nespresso, maar ook hier kwam het
weer terug en ik wil daar meer mee gaan doen! Mensen zijn tijdens het gebruik
van een website, of in dit geval een keukenconfigurator, super kritisch. Maar
op het moment dat je ze gaat vragen naar een cijfer blijkt dat 1 op de 3 maar
echt genadeloos durft te zijn. Of ze zijn zich niet bewust van hun kritische
houding, of misschien wordt de vraag die het geven van een cijfer betreft 
verkeerd geïnterpreteerd, of misschien durven ze gewoon niet streng te zijn 
omdat ze bang zijn dat ze ons ermee pijn doen? Het is in ieder geval slim 
om niet enkel op de uitkomsten van de cijfers te varen. 
<br>
<br>
Helaas ging Claire hierna voor 3 weken op vakantie. Maar ik werd wel (samen met
Iris van het PM) het aanspreekpunt voor Bruynzeel. Ik zie dit als een enorm
schouderklopje voor mezelf! :) 
<br>
<br>
436,75/696
<br>
51/128