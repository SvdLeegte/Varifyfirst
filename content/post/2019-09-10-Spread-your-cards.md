---
title: Spread your cards 
subtitle: to win the game
date: 2019-09-10
tags: ["cardsorting", "creatievetechniek", "nietmetdedoelgroep"]
---
Yes, vandaag stond in het teken van een 'op mijn lijstje staande' creatieve
techniek: CARDSORTING.
Al vaker uit willen voeren, maar helaas niet de juist techniek voor het moment.
Claire (UX Designer) heeft mij dan ook een briefing gegeven over de bedoeling
van deze cardsort sessie. In de ideale UX wereld zouden we deze techniek 
natuurlijk uitvoeren met de doelgroep. Helaas had de klant hiervoor geen budget,
dus het werd met de klant zelf. 
Het lijkt mij interessant om een soortgelijke creatieve sessie met de klant
in de toekomst zelf te organiseren. 
