---
title: Kom maar
subtitle: weer even terug
date: 2019-10-03
tags: ["meeting", "nespresso", "terugkomenmaar", "school", "teruginRoffa"]
---
Eergisteren tijdens de meeting van Nespresso had ik vriendelijk verzocht om 
deze meeting in de ochtend te houden i.v.m. mijn terugkomdag in de middag. 
In principe was het plan om het testscript hier ook nog even door te spreken, 
maar helaas was hier niet genoeg tijd voor. Marktplaats kwam tenslotte ook 
tussendoor. Ik heb mijn PDF dus online op de drive gezet zodat Claire op haar 
gemak er nog even naar kon kijken wanneer ze tijd had. 
Kortom, maandag zal voor een groot deel in teken staan van de marktplaats 
advertenties. Eens zien of ik mijn frustratie op deze manier wel voor me kan 
houden. Het zal toch zeker moeten in de naam van een bedrijf. 
De terugkomdag stond in teken van een case. Je mocht alles bespreken wat een
knelpunt zou kunnen zijn of worden tijdens jouw stageperiode. Ik ervaar een 
zeer open sfeer op stage waar de mogelijkheid bestaat om een gesprek aan te 
gaan waarbij beide partijen eerlijk kunnen zijn, maar ook kunnen accepteren wat
de ander te vertellen heeft. 
Voor mij dus erg moeilijk om een case aan te knopen, omdat als die zichzelf al 
voor zou doen, ik voorheen al heb geleerd dan meteen aan de bel te trekken. 
Misschien is dit het voordeel van al meerdere stages gelopen te hebben. En ik 
denk dat de paardensport daarin de overtreffende trap is als we kijken naar 
stagiaires gebruiken voor eigen gewin. Hier ervaar ik dit dan ook niet. Wel 
een wederzijdse verwachting, maar ik geloof dat een stage ook op die manier 
ingestoken kan worden. 