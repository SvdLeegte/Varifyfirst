---
title: Waarom onderzoeken interessant is
subtitle: soms is het simpelweg een documentaire kijken
date: 2019-09-20
tags: ["onderzoeken", "langsteadem", "pah", "waardevolleopdrachtgevers"]
---
### Deze week heb ik 32,5 uur gewerkt 

Een van de redenen waarom ik erg graag bij Weekend stage zou willen lopen 
is vanwege de impactvolle projecten waar zij aan bijdragen. 
Vandaag kwam er project op mijn bordje betreft PAH, ofwel pulmonale arteriële 
hypertensie. Zoek de rest maar op, daar werd tenslotte niemand dommer van.
Althans ik niet! Uiteindelijk is de intentie om een soort triviant versie te
maken over PAH. Vanuit een enigszins beperkte en redelijk (nog) onduidelijke
briefing kreeg ik de mogelijkheid om een gewenste user journey te maken. Ik kan
je vertellen, hij is nog niet helemaal af, gezien ik tot nog toe wat moeite
heb om een gewenste user journey te maken voor iets wat nog zo weinig concreet 
is. Wellicht is hij op een later moment beter in te vullen! 
### wordt vervolgd.
<br>
<br> 
86,5/696
<br>
8/128