---
title: Nu heb ik de kennis
subtitle: t.o.v. een tweedejaars dan
date: 2019-10-14
tags: ["bezoekje", "wijsneus", "allekennis", "persona", "ookbelangrijk"]
---
Julia kwam gezellig bij mij op bezoek. Mijn functie als peercoach heeft ervoor
gezorgd dat ik precies weet wie Julia als persoon is. Ik was dus ook vooral 
benieuwd waar zij vragen over zou hebben. Ik kan me nog goed mijn voorbereiding
van vorig jaar herinneren. Ik vond het enorm lastig om vragen op te stellen, 
omdat ik ergens ook niet goed wist wat ik moest verwachten. Ik denk dat school
dit vaker terug heeft gekregen want ze hadden dit jaar een hele google docs
vanuit school aangeleverd gekregen. Eigen initiatief staat dus niet meer per se
bovenaan de lijst van school. 
<br>
<br>
Ondanks dat ik wist welk gezicht er bij Nespresso hoorde, was dit nog niet 
gevisualiseerd omdat dit voor mijn gevoel hier niet heel erg veel gebeurd. Dit
is niet erg, want dan creëer ik zit gewoon voor mezelf. Tijd om een persona 
en een empathymap te maken dus. :) 