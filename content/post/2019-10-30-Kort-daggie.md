---
Title: Kort daggie
subtitle: betekent sneller de week doormidden 
date: 2019-10-30
tags: ["wireframes", "twsteel", "feedback", "altijdwaardevol"]
---
Vandaag is enigszins onverwacht een korter dagje. Vanaf een uurtje of 14:30 
zal ik het kantoor verlaten en richting stal moeten vanwege een osteopaat waar
ik al 4 maanden op aan het wachten ben. Enfin, dat is niet echt interessant 
voor deze blog. Deze dag was een mooi moment om verder te werken aan de 
wireframes van TW Steel. Voor vandaag was ik nog een stapje minder ver, want 
eerst moesten er schetsen gemaakt worden om snel een idee over de mogelijkheden
te krijgen. Hierin had ik een enorme hoeveelheid aan ideeën dus het leek me 
slim om eerst even langs Claire te gaan alvorens ik hiermee verder ging. 
Daarbij was ik niet aanwezig in de latere middag, dus was het ook fijn om te 
weten waar ik morgen meteen aan verder kan werken. 
<br>
<br>
De feedback van Claire was een eye opener, dat zeker! Ik had nog onvoldoende 
idee over wat ik waar wilde hebben. Op het moment dat ik dit helderder zou 
krijgen voor mezelf, zou het makkelijk worden over welke indeling van de 
wireframes het beste waren om toe te passen voor onze doelgroep. Nog meer 
tijd nemen -pun intended- om in het merk TW Steel te duiken dus. Het leek mij
handig om dit o.a. te doen d.m.v. een sitemap van de huidige en de gewenste
situatie. Op deze manier hoop ik meer inzicht te verkrijgen over wat ik onder
welk kopje verwacht te vinden, of waarvan ik denk dat men dit onder dit kopje
zou (moeten) kunnen vinden. 
Het staat namelijk ook vrij om de gehele inhoud van de content aan te passen!!
(heel erg interessant, maar dit is voor mij pas de laatste stap...)
