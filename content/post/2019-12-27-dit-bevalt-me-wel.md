---
Title: Dit bevalt me wel
subtitle: minder vierkanten oogjes
date: 2019-12-27
tags: ["minderuur", "kersemis", "ohdenneboom", "toedeloe", "bijna2020"]
---
### Ik heb 24,25 uur gemaakt deze week. 

Oh jeetje, wat is het rustig, en dat is alles behalve een understatement. Deze
hele week heb ik maar 3 kleine opdrachtjes in mijn agenda gekregen. Maar dat 
vond ik niet zo'n probleem want daardoor heb ik deze week goed aan mijn 
stageverslag kunnen zitten. Oh, en YouTube was deze week mijn beste vriend. 
<br>
<br>
Wat me wel bevalt? Minder dan 5 dagen werken. Voorheen heb ik 6 dagen in de 
week gewerkt, en daar heb ik nooit problemen mee gehad. Maar 5 dagen achter
een computer zitten vind ik echt teveel. Ik zou prima 6 dagen kunnen werken, 
waarvan 4 achter de computer en 2 'on the road'.

