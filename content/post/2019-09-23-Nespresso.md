---
title: Nespresso
subtitle: what else 
date: 2019-09-23
tags: ["nespresso", "whatelse", "snapjedeclooney", "slechtewoordgrappen", "meeting"]
---
Inlezen, meeting en schetsen en dat allemaal voor dezelfde naam: *Nespresso*.
Een absolute inspiratievolle dag, en nee ik ben niet sarcastisch. Soms is het 
nodig om dit te vermelden. ;) 
Ik merk dat ik een meeting super interessant vind maar nog wel zoekende ben 
naar de hoeveelheid input die ik kan/mag geven. In de toekomst zou dit nog wel
eens een interessant puntje zijn om te bespreken naderhand. Dit betrof een 
meeting intern, waardoor ik makkelijker mijn vragenvuur durfde te openen dan 
bij een opdrachtgever. Wellicht loopt dit vanzelf los, afhankelijk van het
moment van instappen in een project. 
Om deze dag nog even af te sluiten ben ik zelfonderzoek gaan doen omtrent UX
principes. UX is natuurlijk erg breed, en ik wil mezelf graag beter kunnen
omschrijven naar potentiële klanten in welk vakgebied ik mezelf (mogelijk) wil
gaan vestigen. 
