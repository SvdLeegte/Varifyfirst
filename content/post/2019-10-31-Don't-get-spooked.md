---
title: Don't get spooked
subtitle: it's only work. ;) 
date: 2019-10-31
tags: ["halloween", "doeiknietaan", "sitemap", "huidige", "gewenste"]
---
### Ik heb deze week 40,5 uur gewerkt

Zoals gisteren al gezegd is het vandaag tijd voor de sitemap. Huidige situatie
is redelijk snel gemaakt, alleen de gewenste situatie heeft net iets meer 
tijd nodig. 
Ook hiervoor ben ik opnieuw nog eens verschillende websites hetzelfde
gebruikersdoeleinde gaan bekijken. Wat zetten zij bijvoorbeeld onder stories? 
Welke structuur heeft de huidige website, en wat van die structuur zou ik 
onder het kopje stories kunnen schuiven? Wat is een logische opdeling van de
categorieën? Genoeg dingen om over na te denken... 
<br>
<br>
322.25/696
<br>
28/128
