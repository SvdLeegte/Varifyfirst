---
title: Theeleuten met bezoek erbij
subtitle: en feedback
date: 2019-10-23
tags: ["theeleuten", "stagebezoek", "feedback", "emotioneelmomentje"]
---
Oprecht, ik was best wel benieuwd naar dit bezoekje. Vooral omdat ik vandaag
ook mijn feedback terug zou ontvangen, maar ook naar de mogelijkheden die nog
gecreëerd gaan worden om mijn leercurve maximaal te benutten. 
Mijn geluk kon niet op. Ik heb super uitgebreid (wat ik al niet per se had 
verwacht.. niet omdat ik denk dat Claire daar niet voor gaat zitten, maar omdat
ik denk dat school hier best wel veel vraagt als het om het feedback geven gaat
omtrent de competenties...) feedback mogen ontvangen van Claire. Wat opzich al
winst nummer 1 is. Maar ook echt enorm positief! Waar ik zelf nog duidelijk wel
wat meer verbetering zou zien, krijg ik hier al hele positieve feedback terug. 
Het stukje waar ik me in kan herkennen is dat ik meer en sneller mag gaan 
vragen, i.p.v. eerst af te wachten. Ik zoek graag zelf dingen uit, maar blijf
hierin soms te lang zwemmen, terwijl ik makkelijk en snel antwoord kan krijgen
vanuit Claire of Mark, of misschien soms een andere expert binnen Weekend. 
<br>
<br>
Verder heb ik vandaag de laatste inzichten betreft Nespresso met Claire 
gedeeld zodat deze nog toegevoegd konden worden in de presenatie van de pitch.
Voor een pitch die je nog moet winnen wil je het wel in een mooi jasje gieten.
Dit maakt dat je vooral de punten pakt die je goed in kunt zetten en kunt
verantwoorden. Claire heeft wel op mijn hart gedrukt: normaal gesproken deel
je wel echt alle inzichten. We willen namelijk winst behalen, en niet enkel 
de goede dingen benadrukken. Wij zijn er tenslotte om hetgeen dat beter kan 
te onderzoeken. Als je alles goed hebt, dan hoef je niet meer te onderzoeken. 
Hier wil ik mezelf wel aan vasthouden eigenlijk, mooi motto is dat. 