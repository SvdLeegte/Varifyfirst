---
title: Kiezen doe je 
subtitle: onder het genot van koffie
date: 2019-09-18
tags: ["bruynzeelkeukens", "meeting", "koffie", "nespresso"]
---
Bruynzeel kwam op mijn eerste dag op mijn bordje van opdrachtgevers. 
Vandaag mocht ik mee met de meeting waar we de uitkomsten van de expert review
terug gingen leggen naar de opdrachtgever. 
Zeer leerzaam, want hier kwam een extreme miscommunicatie naar boven. 
<br>
<br>
In de namiddag had ik nog de tijd om in een nieuwe opdracht te duiken. Eind
oktober hebben we de mogelijkheid om een lange termijn opdracht voor Nespresso
binnen te halen. Hier heb ik de mogelijkheid gekregen om mee te lopen in 
dit traject. 
