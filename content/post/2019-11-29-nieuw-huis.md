---
title: Nieuw huis
subtitle: nieuwe verwarming
date: 2019-11-29
tags: ["verhuizing", "nieuwepotten", "ontheroad", "literatuur"]
---

### Ik heb deze week 41,25 uur gewerkt

Genieten, de weg op, en dan ook nog naar Intratuin! Deze week stond vooral in 
het teken van de verhuizing en zelfstudie. Ik ben iedereen binnen kantoor 
afgegaan om te vragen of ze nog iets te doen hadden, maar dat was niet het 
geval. Aangezien ik nog weinig voelde om vollop aan mijn stageverslag te werken
(misschien krijg ik hier later spijt van, maar zorgen zijn voor later) heb ik
vooral het boek 'Don't let me think' verder gelezen, en andere informatie
(waaronder op YouTube) die op dat moment mijn interesse trok. 
<br>
<br>
Waarom is de Intratuin zo achterlijk ingericht? Ze hebben de bloempotten door
de hele zaak staan, waardoor ik aan het einde weer helemaal terug moet omdat 
hetgeen wat achterin de zaak is, toch niet is wat het is. Uiteindelijk maar 
naar de Hornbach gegaan omdat ik het aanbod bij de Intratuin toch niet heel 
erg interessant vond. 
<br>
<br> 
Deze week heb ik ook een moment gehad waarin ik brak. Ik heb de hele week lopen
sjouwen en lopen doen, wat ik echt geen probleem vond. Echter kreeg ik niet 
alle planten in mijn eentje verpot (waaronder een cactus), en vroeg ik mijn
collega's 5 minuten van hun tijd. Helaas had niemand die 5 minuten tijd, ondanks
dat ik het via slack, persoonlijk en nog eens 'klassikaal' heb gevraagd. Maar
één persoon gaf aan dat ze geen zin had, de rest had 'geen' tijd. Ik vond het
jammer dat ik zoveel moeite steek in alles en iedereen, en dat ik dan niet eens
een eerlijk antwoord terug krijg... Want uiteindelijk had niemand zin, want wie
kan er nu 8 uur per dag volledig op zijn scherm gefocused blijven, zonder 5 
minuten naar iets anders te kijken? You tell me.... 
<br>
<br>
478/696
<br>
60/128