---
title: Wire the frame
subtitle: of diabetes
date: 2019-09-11
tags: ["wireframe", "diabetes", "projectomhetverschiltemaken", "brainstorm"]
---

Er wordt hier top ingespeeld op mijn leerdoelen. Een van mijn leerdoelen is 
dat ik aan projecten wil werken die het verschil zouden kunnen maken. 
Vandaag stond dan ook voor een deel in het teken van diabetes. Om meer kennis
te verwerven wilde een klant een app hebben waar men de mogelijkheid heeft 
om meer te weten te komen over diabetes type 1 - het type dat niet te genezen
is. 
<br>
<br>
### Hoe kan ik de interface van een applicatie indelen waar men voor een dag kan ervaren hoe het is om diabetes type 1 te hebben?
Deze interface was nog niet per se hoe het precies elkaar op zou volgen, maar 
meer een creatieve brainstorm over de mogelijkheden om een eerste indruk te 
geven aan de klant. 
<br>
Wanneer men de bloedsuikerspiegel meet wordt dit weergegeven d.m.v. een golf. 
Dit heb ik uiteindelijk dus meegenomen in het ontwerp welke Dennis visueel 
heeft uitgewerkt. 
