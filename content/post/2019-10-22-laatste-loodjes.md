---
Title: laatste loodjes
subtitle: en dan zijn de resultaten echt verwerkt hoor.
date: 2019-10-22
tags: ["laatsteloodjes", "resultaten", "nespresso", "whatelse"]
---
Ondertussen zie ik overal gesponsorde reclames op mijn feed voorbij komen, 
kan ik niet meer om de reclame in de normale winkel heen, en weet ik alles 
over Nespresso. Maar ik kan je vertellen dat ik er ondertussen ook wel een 
beetje klaar mee ben. Gelukkig staat overmorgen de pitch op de planning. 
Spannend, want gaat dit hetgeen zijn wat ze van ons verwachten? Best wel extreem
onderschat om de resultaten op een goede manier te verwerken/rapporteren.
<br>
<br>
Tijdsprong. Na de pauze ben ik maar meteen boodschappen gaan doen. Nee, dat is 
een grapje. Ik ben wel naar de supermarkt gegaan, en stiekem ook een stukje 
boodschappen maar dat was meer omdat ik ongemakkelijk van mezelf werd. Met
mijn field research naar Saitaku ben ik foto's gaan maken van redelijk veel 
producten in zowel de Albert Heijn, de Toko Toko als de Jumbo. Dit maakte dat ik
mezelf enigszins verplicht voelde om iets te kopen, voordat ze me als gekkie 
bestempelde en me een supermarkt verbod op zouden leggen omdat ze bang waren 
dat ik iets zou gaan stelen? Geen idee, misschien zie ik spoken. Maar als ik 
een medewerker ben van een supermarkt, dan zou ik het oprecht best vreemd vinden
als iemand alles staat te fotograferen en vervolgens niks koopt. 