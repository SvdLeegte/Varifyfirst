---
title: change of plans
subtitle: van Sligro naar Nespresso
date: 2019-10-02
tags: ["sligro", "nespresso", "verandering", "prioriteitenstellen"]
---
Gezien Sligro en Nespresso ergens wel overeen komen met de ontwerpvraag maar 
Nespresso momenteel de prioriteit heeft, is het tijd om een testscript te 
schrijven op basis van het prototype van Nespresso. Wat mij hierin vooral heeft
geïnspireerd, is dat ik een van mijn leerdoelen aangepast heb naar EVO omdat 
ik ineens het nut van een testscript zag. 
Ik heb altijd het gevoel gehad dat het overbodig was, en geen toegevoegde 
waarde had, maar zoals ik hem nu heb kunnen schrijven is het wel degelijk van
toegevoegde waarde. Claire en Mark hebben mijn testscript van feedback voorzien, 
waarna ik een verbeterslag kon maken. 
<br>
In de meeting van Nespresso werden veel dingen vooral bevestigd... Dat er veel
getest moet worden, en hoe we de komende weken in gaan plannen tot aan de pitch.
Wat hebben we echt nodig om de pitch binnen te kunnen harken? Waar zien we 
mogelijkheden, en wat willen we juist uit de weg zodat we ze niet tegen de 
schenen aanschoppen.
<br>
Claire maakte bijvoorbeeld al de opmerking dat we het huidige design niet willen
testen, omdat Nespresso zelf hoogstwaarschijnlijk al een hoogwaardiger onderzoek
heeft verricht dan wij, nu in ieder geval, kunnen doen... 
En ik denk dat daar wel wat inzit, maar toch is het jammer dat ik op deze manier
geen resultaten tegenover elkaar kan leggen en met cijfers kan bevestigen dat 
het huidige resultaat beter is dan het oude. 
<br>
Al met al, mijn testscript gaat een knaller zijn t.o.v. al mijn eerder gemaakte
versies. Daar gaat het uiteindelijk om, al doende leert men, toch? 