---
title: Planning bijstellen
subtitle: en soms je verwachtingen
date: 2019-11-15
tags: ["planning", "verwachtingen", "laatsteaanpassing", "moeilijk"]
---
### Deze week heb ik 40,25 uur gewerkt
Ik heb besloten deze week in 1 blogpost te zetten omdat ik eigenlijk maar een 
klein aantal punten aan kan stippen binnen mijn leerproces. 

### TW Steel
Voor TW Steel moest nog een mail opgesteld worden ter ondersteuning van de 
wireframes die ik gemaakt had. Nu was het even de vraag of Claire dit zou doen, 
of dat ik dit zou doen. Ik heb gezegd dat ik er graag een poging aan wil wagen
maar of ik daarvoor wel een aantal richtlijnen zou kunnen krijgen. 
Het was eigenlijk niet zo moeilijk, enkel merkte ik dat ik hier en daar nog 
wat moeite had met de juiste begrippen te gebruiken. 'UX gezien de beste 
oplossing' is natuurlijk gewoon 'conventioneel de beste oplossing'. 
Uiteindelijk met een paar kleine aanpassingen is deze mail goedgekeurd 
en doorgestuurd naar de klant. 

### VIA VAI 
In principe was het de bedoeling dat ik parallel zou gaan schetsen voor de 
website van VIA VAI. Ook was dit een goede mogelijkheid om aan ideevorming te 
werken. Ik heb hiervoor dan ook een moodboard en een mindmap gemaakt. Vanuit
hier heb ik voor mezelf een ontwerpvraag opgesteld aangezien ik ervoor wilde 
waken dat ik teveel zou gaan zwemmen en op deze manier voor mezelf een kader 
kon stellen. 
Helaas heb ik hiervoor het grootste gedeelte van het project moeten laten 
vallen aangezien TW Steel natuurlijk voor ging en ik hierin niet heel erg 
goed heb gecommuniceerd over de deadline. Wat wel erg jammer was, want VIA VAI
was geen verkeerde aanvulling geweest op mijn leerproces. Hierin kon ik namelijk
mijn kennis die ik opgedaan had bij Nespresso en TW Steel samen laten komen
om mogelijk sneller te kunnen werken en het proces beter bij te houden. 

### Bruynzeel Keukens
Buiten de velen DTP opdrachtjes deze week kwam er aan het einde van de week
ineens onverwachts iets nieuws & leuks op de planning! Bruynzeel Keukens had 
getekend voor een gebruikersonderzoek en ik mocht hieraan meehelpen! 
Tijden Nespresso was er wel een soort van vluchtige expert review gedaan, maar
dit was voor mij ook allemaal heel erg nieuw en ik wist nog niet zo goed hoe
of wat, maar dit keer wist ik dit wel! Of in ieder geval beter. 
Ik ben dus mijn eigen expert review gaan schrijven, wat al veel sneller ging 
dan de vorige keer (wat ik ook terug kreeg van Claire). Daarbij kwamen er 
heel veel punten overeen met de expert review van Claire, wat ik ergens als 
compliment zie omdat ik redelijk dezelfde kijk op dingen heb. Wat je aan de 
andere kant zou kunnen stellen is dat we er op dezelfde manier naar kijken, 
omdat er geen 1 echte richting is voor een expert review. 
Je kunt tenslotte een expert review op verschillende fronten invullen: enkel
iconen, consistentie of misschien zelf op een ander gebied. Ik heb me niet 
per se op 1 ding gefocust. Ik heb me vooral proberen op te stellen als een 
gebruiker en vanuit daar te gaan klikken en vooral te ervaren. Wat valt me op? 
Wat begrijp ik niet, en wat zou de gemiddelde gebruiker mogelijk niet kunnen
begrijpen? 
<br>
<br>
396/696
<br>
32/128