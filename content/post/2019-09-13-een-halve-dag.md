---
title: Een halve dag 
subtitle: is ook een werkdag
date: 2019-09-13
tags: ["terugkomdag", "drukgenoeg", "treingemistopdeminuut"]
---
### Ik heb deze week 29,5 uur gemaakt

Om mijn gemiste dagen van het introkamp even recht te trekken, heb ik besloten
om de ochtend gewoon lekker te werken als ik terugkomdag heb. Ik woon tenslotte
maar 3 minuten verderop en het is op de weg naar het station, beter kan niet!
### Toch?!
Om de inzichten van de cardsortingsessie niet volledig te laten wegzakken heb 
ik mijn tijd genomen om deze even op een rijtje te zetten en te delen met Claire.
Daarbij kreeg ik de opdracht vanuit Iris om twee presona's te maken. Dit werd 
uiteindelijk een dusdanige race tegen de klok dat ik mijn trein gemist heb.
Nee, ik had dat kopje koffie voor onderweg écht nodig!! 
<br>
<br>
Oh die hoeveelheid eten die ik in de ochtend bij de Appie had gehaald heb ik 
gedeeltelijk op het kantoor laten liggen voor mijn collega's. Stukje 
wederzijdse waardering, toch? ;) 
<br>
<br> 
54/696
<br>
0/128