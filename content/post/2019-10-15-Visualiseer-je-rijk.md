---
title: Visualiseer je rijk
subtitle: met Nespresso
date: 2019-10-15
tags: ["nespresso", "whatelse", "persona", "empathymap"]
---
Ik hoor je denken: 'WHAT ELSE?!'. Ja, precies ik had dezelfde gedachten toen ik 
dit op aan het schrijven was. Nespresso heeft inderdaad heel veel voor mijn
ontwikkeling als designer gezorgd. Gisteren kreeg ik mijn persona en empathymap
niet helemaal af, dus vandaar dat dit vandaag terug komt. 
<br>
<br> 
Wat stond er vandaag verder nog op het programma?! De battle of the socks,
wiehoeeee. Dit mocht dan ook gepresenteerd worden. Waar ik heel enthousiast 
van werd aangezien dit weer een ontwikkelingspuntje is in mijn 
presentatietechnieken. Wellicht heb je nu even je vraagtekens wat 'battle of the
socks' inhoudt? Nou, het gaat om het eindejaarspresentje voor relaties van heel
Handpicked. Ik had samen met Rowie de mogelijkheid om 2 labels te kiezen en 
daar sokken voor te ontwerpen. Na wat brainstormen en kijken wat de labels
allemaal inhielden was het tijd om de knoop door te hakken: ik ging voor
bluebirdday en boldly xr. Toch wil ik hier morgen nog even aan werken. Oh, 
voordat ik het vergeet... de bakker begint met te herkennen! Er mocht namelijk
weer wat lekkers gehaald worden voor in de pauze. Tot zover mijn 'in mijn 
stageperiode ga ik gezonder eten dan op school'. 
