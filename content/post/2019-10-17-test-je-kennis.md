---
title: Test je kennis
subtitle: met een prototype
date: 2019-10-17
tags: ["prototype", "usabilitytesting", "testen", "nespresso", "design"]
---
Yes, tijd om de laatste spullen te verzamelen en richting Handpicked te gaan. 
Daar gaan we tenslotte de test afnemen met 3 participanten. Checklist
voor de laatste keer gecheckt, alles bij elkaar verzameld en GO! 
Hier hebben Claire en ik nog de laatste check gedaan op het prototype, en ja 
hoor technical problems are showing up. Er gebeurde iets in het prototype wat
niet hoorde... Dit moet eerst gefixt worden vanuit Weekend alvorens we de test
konden starten. We liepen dus even uit. Claire vroeg aan mij of ik wilde 
beginnen met het interviewen en afnemen van de test. Echter vond ik het een
beter plan om haar voor te laten, te observeren hoe zij het aanpakte om 
vervolgens de andere participanten zelf af te nemen. Ik merkte dat het 
testscript echt een goede fundering was voor deze test. En dat er nog veel
dingen waren die ik graag wilde testen, maar dat ik het soms lastig vond om 
het op een manier te vragen zonder het teveel in te vullen. Ik denk dat het
ook veel zal helpen als de volgende keer meer knoppen klikbaar zijn. 
Nu was het vooral veel in detail uitgewerkt waarbij eigenlijk 3 primaire acties
getest konden worden. Omdat het prototype visueel al erg ver uitgewerkt was 
denk ik dat de verwachtingen anders liggen dan bij bijvoorbeeld een paper 
prototype. Men weet daarbij namelijk dat het niet clickable is. En hier 
weten ze niet wat er clickable is, en gebeuren er daardoor mogelijk dingen
enigszins onverwacht. En als het gebeurd en ze willen het nog een keer 
proberen, dan is de interactie al voorbij omdat hij enkel 1x geanimeerd is. 
Dit zijn mooie dingen om mee te nemen naar een volgende test. Idealiter zou je
vaker en eerder in het proces al testen, enkel was dit voor de pitch 
niet echt mogelijk. Hier ligt wel een heel groot deel van mijn interesse als
UX Designer merk ik. Toch wil ik nog veel verbeteren in mijn interview skills. 
Claire was hier wel erg enthousiast over. Maar kritisch op jezelf zijn wordt
niemand slechter van denk ik. :)