---
title: D'n letste dan
subtitle: omdat het ooit de laatste keer moet zijn
date: 2019-11-08
tags: ["letste", "nogeenbeetjetweaken", "omjevingersbijaftelikken"]
---
### Deze week heb ik 33,5 uur gewerkt
Grappig om zelf ook een stukje van je groei te kunnen ervaren. Doordat ik bij 
TW Steel zo 'de mist in' was gegaan betreft het onderzoeken naar de juiste
interface en wireframes, pakte ik het op een totaal andere manier aan bij VIA
VAI. Hier ben ik bijvoorbeeld meteen op gaan schrijven wat ik op elke pagina
wilde hebben. Samen met mijn moodboard en wordweb kon ik al best een eindje 
komen in mijn ideeën voor een ontwerp. 

Oke, toch nog even aan TW Steel. VIA VAI kwam tussendoor en was eigenlijk ook 
wel weer even fijn als afwisseling. Ik merk dat als ik te lang bij een project
blijf hangen dat ik dan niet echt meer vooruit kom... Dan is mijn fles vol 
inspiratie ineens helemaal leeg gelopen. 
Enfin, er was nog wel het een en ander recht te zetten op de wireframes voordat
ze geëxporteerd konden worden. 
Die finalfinalfinal.sketch versie, wittewel? 
<br>
<br>
355,75/696
<br>
32/128



