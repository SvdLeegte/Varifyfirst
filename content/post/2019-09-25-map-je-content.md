---
Title: Map je content
subtitle: om te weten wat je hebt
date: 2019-09-25
tags: ["contentmapping", "hoemoetdat", "zelfonderzoek", "tochishulpfijn"]
---
Nespresso is een internationaal bedrijf, dat wil zeggen dat zij dus ook 
een ander soort jargon gebruiken. Uit onderzoek blijkt dat dit dus helemaal
niet aansluit bij de Nederlandse markt, want deze markt kent geen ristretto. 
In Nederland is een ristretto een espresso, terwijl een espresso in het 
buiteland gelijk staat aan een lungo, ofwel koffie, in Nederland. Snap jij het
nog? Nou, dat dus... Tijd voor een stukje contentmapping. Nog nooit gedaan, dus
voordat ik aan de bel trek vind ik het altijd leuk om te kijken of ik er 
binnen een niet al te lange tijd zelf uit kan komen. Na een 15 minuten wat 
googlen, heb ik aan Mark even een voorbeeldje gevraagd. Deze heb ik gekregen 
en naar mijn eigen idee geïnterpreteerd om vervolgens van feedback te laten 
voorzien en dan eventueel nog een verbeterslag toe te passen. Tot mijn enorme
verrassing, waren er geen op- of aanmerkingen op mijn contentmap! Blijkbaar
was mijn benadering van het probleem de juiste manier om dit probleem aan te 
vliegen. Eentje voor in de boeken! 