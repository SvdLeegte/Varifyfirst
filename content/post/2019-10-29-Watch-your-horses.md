---
title: Watch your horses
subtitle: cause this is the start of something new 
date: 2019-10-29
tags: ["something", "new", "watchyourhorses", "watches", "onlyformen"]
---
Tijd om richting Claire te gaan voor de kickoff van deze wireframes. Blijkbaar
heeft TW Steel al een design staan, en zijn de eerste wireframes ook al 
high-fid uitgewerkt. Echter missen zij nog 3 wireframes voor de volgende 
kopjes: stories, about us & contact. Nou, je raad het al, aan mij de taak 
om deze wireframes verder uit te werken. Alvorens ik aan de schetsen kan 
beginnen is het niet onverstandig om even wat onderzoek te doen naar het merk
TW Steel. Daar hoort ook een stukje inspiratie bij. Er zijn niet direct 
beperkingen waardoor ik volledig los kan gaan op dit ontwerp. Wat het 
tegelijkertijd ook wel erg lastig maakt. Het is dus handig om op tijd voor 
de eerste feedback bij Claire langs te gaan om te kijken hoe ik ervoor sta, 
en om ervoor te zorgen dat ik niet teveel ga zwemmen. 
De lijn tussen mijn concentratieboog die verdwijnt en het stukje dat ik niet 
goed weet hoe ik ermee verder moet ligt heel erg dicht tegen elkaar aan. 
Het is dus handig om een manier te vinden voor mezelf om te weten wanneer het 
mijn concentratieboog betreft, of wanneer het 'verloren in het project' 
betekent. PVA'tje is best handig hiervoor. 