---
title: Final countdown
subtitle: want de deadline komt eraan
date: 2020-01-10
tags: ["pah", "stageverslag", "nieuwproject", "mindercmd"] 
---

### Deze week heb ik 41,5 uur gemaakt

Oke, het laatste weekje voor het stageverslag. Mijn planning voor dit verslag 
is eigenlijk heel goed verlopen. Zo had ik aan het einde van deze week mijn 
verslag tekstueel af. Deze week staat o.a. voor het vullen van de content. 
<br> 
<br> 
Maar dat is niet het enige wat gevuld moet worden met content. De Belgische 
website over PAH (Pulmonale arteriele hypertensie) moet ook gevuld worden 
met content. Dit project stond al weken geleden in mijn agenda voor 40(!) uur
en het moest af voor het weekend. Aangezien dit woensdag binnen kwam, kreeg ik 
het een beetje warm... 
<br>
<br>
Gelukkig was de website van de Nederlandse versie volledige gekopieerd dus was
het enkel wat content aanpassen. 
<br>
<br> 
Hoewel ik het helemaal geen probleem vind om met mijn eigen ding bezig te zijn
vind ik het de laatste weken wat een beetje jammer dat ik niet meer echt 
opdrachten heb gehad die betrekking hadden op CMD. Komende week ga ik daar 
nog even verandering in brengen. Dan ga ik, als er geen andere opdracht meer
ligt, een website ontwerpen voor mijn eigen bedrijf. Wanneer ik hier een 
ontwerp voor heb liggen wil ik eens sparren met Claire of Mark. 
<br>
<br> 
Maar voor nu ga ik focussen op mijn verslag... Want ik heb nogal wat puntjes 
op de i te zetten voordat ik hem in kan leveren. 
