---
title: Toet toet toeter
subtitle: in de Fiesta scooter
date: 2019-09-09
tags: ["campagne", "Mechelen", "nietuuteren", "gasgevenanderskomtdespits"]
---

Deze maandagmorgen stond in teken van een PDF correctie en verder vooral 
ballonnen en helium. Voor een campagne moesten er ballonnen naar locatie ter
ondersteuning van het reeds geplaatste promotiemateriaal. 
Dus, zo gezegd zo gedaan: 72 balonnetjes opblazen, knopen om vervolgens 12 
stuks weer aan elkaar te knopen tot een geheel.
<br> 
<br>
Vervolgstap? Proberen die ballonnen niet te laten klappen terwijl ik in de 
auto onderweg naar de klant. En dit is een stukje, waar een van de twee 
locaties Mechelen is. Tijd om gebruik te maken van de linkerbaan, want om 
16 uur begint de spits alweer in Antwerpen op de ring. 
<br>
<br>
# Joejoeee, OMW