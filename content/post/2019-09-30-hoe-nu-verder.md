---
title: Hoe nu verder
subtitle: om te bepalen wat het juiste is
date: 2019-09-30
tags: ["meeting", "nespresso", "whatelse", "sligro", "watmoetiktesten"]
---
Wat komt er allemaal bij kijken van het schrijven van een testplan? Voor 
Sligro ben ik gaan kijken welke test er gebruikt kan worden om de 
merkbeleving en gebruiksvriendelijkheid te testen. In dit geval ben ik gaan 
kijken naar de 5 second test om zo de eerste merkbeleving te testen. Wat ziet
men? Wat valt hen op? Wat is het eerste gevoel dat ze krijgen wanneer ze de
afbeelding zien die hen gepresenteerd wordt? 
Daarbij ga ik kijken waar ik benieuwd naar ben betreft het gebruik van de 
website. Is de cta duidelijk? Waar gaan ze als eerste naartoe? Moet de gebruiker
nadenken bij het gebruik van de website? Dit gaf mij al snel input om te gaan 
kijken naar het boek 'Don't let me think!'. Dit boek is geschreven betreft 
de gebruiksvriendelijkheid van o.a. websites. 
Elke dag brengt me iets nieuws om te leren. Krijg ik dat niet aangeboden, dan
ga ik er zelf naar opzoek. :)