---
title: Test rapport
subtitle: is ook een rapport
date: 2019-10-18
tags: ["testrapportage", "veelinzichten", "verwerken", "data"]
---
### Ik heb deze week 40,75 uur gewerkt
Zo, tijd om al de data en inzichten te gaan verwerken in een overzichtelijk
rapport. Hiervoor ben ik gaan kijken hoe men een overzichtelijk rapport 
kan opstellen dat men wil lezen. Claire heeft me gewaarschuwd dat het wel 
veel tijd kost. En uiteindelijk toen ik het in een word document op een rijtje
aan het zetten was, dat het ook een idee is om het in een excel bestand op 
een rijtje te zetten. Ik heb dan dus ook het testscript erbij gepakt, en
de vragen die we gesteld hebben op een rijtje gezet. Vervolgens heb ik per 
participant antwoord gegeven op de gestelde vraag waardoor er een erg groot
maar enigszins overzichtelijk bestand. Veul werk wel. 
<br>
<br>
241,5/696
<br>
15/128