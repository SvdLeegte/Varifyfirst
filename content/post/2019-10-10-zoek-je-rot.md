---
title: Zoek je rot
subtitle: naar rechthoekige post-its 
date: 2019-10-10
tags: ["post-it's", "zoeken", "twintigwinkelsverder"]
---
Ja, aan de titel raad je het al, dit wordt weer een kwaliteitspost. De laatste 
planning voor Nespresso is uitgewerkt, dus we weten wat ons te doen staat tot 
de deadline van de pitch. In de middag kwam ik erachter dat rechthoekige post-it's
verdomd moeilijk te vinden zijn. Zelfs in de avond met koopavond ben ik nog 
in verschillende winkels geweest om mijn missie te voltooien, maar helaas
tevergeefs... Deze moeten in de toekomst toch maar even besteld worden. 
