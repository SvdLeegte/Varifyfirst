---
title: Hoe voelt dat
subtitle: op school zijn? 
date: 2019-12-13
tags: ["terugkomdag", "dnletste", "twsteel", "workshop", "lambweston"]
---

### Deze week heb ik 37,5 uur gewerkt

Dit was een hele nuttige en interessante week. Zo ben ik aan de slag gegaan
met de dekstop versie van de wireframes voor TW Steel. Hierin merkte ik 
hoeveel makkelijker het is om voor desktop te ontwerpen t.o.v. mobile. Daarom
is het dus handig om mobile first te ontwerpen. Als het op mobiel werkt, dan 
krijg je het meestal ook wel werkend op desktop. 
<br>
<br>
Verder heb ik de workshop van Lamb Weston mee mogen faciliteren. Hierin heeft
Iris verschillende creatieve sessies opgezet waarin een veelheid aan ideeën 
gegereneerd moest worden. Deze sessie werd in het Engels gehouden, dus ik heb
hierin mezelf goed in het Engelse kunnen presenteren. 
<br>
Verder heb ik vooraf aan de creatieve sessie een drietal aan presentaties 
kunnen observeren. Dit was handig voor mijn competentie samenwerken 
waarvoor ik nog graag zelf een concept pitch aan de opdrachtgever. Helaas 
is de kans erg klein dat dit nog gaat gebeuren voor de deadline van het 
stageverslag. 
<br> 
De terugkomdag was fijn voor het bespreken van de feedback op mijn verslag. 
Hierdoor heb ik weer genoeg nieuwe handvaten waar ik mee verder kan. Toch ben
ik nog niet helemaal overtuigd van mijn opzet voor het stageverslag. Ik wil nog
erg graag veel feedback vragen. Helaas is dit lastig omdat ik geen van mijn
vriendin mijn stagegroep heb zitten, en ik verder moeilijk contact heb met 
de andere mensen die wel in mijn stagegroep zitten. 
<br>
<br>
557/696
<br>
75/128