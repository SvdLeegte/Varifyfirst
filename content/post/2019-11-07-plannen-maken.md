---
title: Plannen maken
subtitle: en flexibel zijn
date: 2019-11-07
tags: ["plannen", "vrijstaandmaken", "minimaalzoveelstarrts", "evenschikken"]
---
Oke, ik had in mijn agenda dus een enorm idealistisch doel gesteld. Aan het
einde van deze week wilde ik namelijk 2 STARRTS afhebben inlcusief (!!) de 
bewijslast. Aankomend weekend ga ik namelijk naar de Ardennen om even te 
ontluchten van al die studentenstressssss. 
Nu zul je zien, dat wanneer je zulke concrete, en niet flexibele doelen voor
jezelf stelt... dat er veel tussendoor komt. Op de dag dat ik mijn 
tekentablet niet in mijn tas heb zitten, krijg ik namelijk de vraag of ik een 
afbeelding vrijstaand wil maken.... Vroeger vond ik het top om manipulaties 
te maken, en daarmee dus afbeeldingen (meestal paarden) vrijstaand te maken. 
Ik kan je vertellen, dat mijn mening daar enigszins over bijgesteld is. 
Alle tools die we tot onze beschikking hebben, zelfs de nieuwe object selection
tool van Photoshop werkte natuurlijk niet op mijn afbeelding. 
Toch maar even lekker met een mask gaan gummen. Klein detail: het komt op een 
banner van 2 meter.... Dus secuur werken is geen overbodige luxe. 

Enfin... Daar komt natuurlijk bovenop dat ik mijn research voor VIA VAI even 
op moest schuiven. Dit vond ik trouwens wel een mooie gelegenheid om aan mijn
competentie ideevorming te kunnen werken. 
Ik ben absoluut heel erg blij met deze nieuwe opdracht, en dan schuif ik het
schrijven van mijn STARRT eigenlijk zonder problemen op. Helaas weet ik alleen
nog niet hoe volgende week gaat verlopen, want voor het einde van volgende 
week heb ik tenslotte het schrijven van 4 STARRTS op de planning (inclusief 
bijlagen).
Hoptjop gas erop is de enige optie die nog open staat dan, toch? 