---
title: Soms heb je ze
subtitle: en soms moet je ze genereren
date: 2019-12-20
tags: ["ideeen", "creatievesessie", "technieken", "probeerietsnieuws"]
---
### Deze week heb ik 40,75 uur gewerkt.

Afronden van TW Steel, want vandaag bestond pas de mogelijkheid om aan een UX
designer feedback te vragen. Deze waren namelijk einde vorige week allemaal
niet aanwezig op kantoor. Enfin, dit is nu afgerond en richting de klant! 
Verder was het aardig druk, maar daarom hebben Rowie en ik een moment in onze
agenda gezet voor een creatieve sessie. Anders komen we nooit aan ideevorming 
toe natuurlijk! Dit was erg leuk om te doen, want ik heb heel veel kunnen kijken
naar creatieve technieken die ik nog nooit eerder heb gedaan. Daarbij was onze
opdrachtgever Weekend zelf, dus konden we goed de grenzen opzoeken betreft de 
ideeën. We hebben hier de hele dag voor ingeroosterd zodat we ook een 
vervolgsessie konden organiseren. Hierin heb ik de creatieve technieken 
uitgeschreven, en geselecteerd wat voor ons van toepassing is. 
<br>
<br>
597,75/696
<br>
80/128