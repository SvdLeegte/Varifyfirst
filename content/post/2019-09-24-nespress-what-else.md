---
title: Nespresso what else
subtitle: part II
date: 2019-09-24
tags: ["schetsen", "onderzoeken", "itereren", "nespresso", "whatselse", "partII"]
---
Yes, part II van Nespresso! 
Vandaag kon ik verder aan Nespresso. Dit keer ben ik gaan kijken naar de 
flow betreft het bestellen van een cupje Nespresso. Wat zou hierin beter kunnen,
en wat kan ik meenemen? 
Hierin heb ik verschillende schetsen gemaakt waarvan enkele uiteindelijk zijn
zijn opgenomen in de wireframes. Dit heb ik teruggelegd bij Claire waar we 
samen nog even hebben zitten sparren over de indeling. We lopen momenteel een
beetje tegen het probleem dat we mogelijk tussen commercie en gebruiksvriendelijkheid
moeten gaan kiezen. Daarbij willen we niet te 'cockie' zijn aangezien we denken
dat Nespresso zelf over heel veel dingen al heeft nagedacht (en mogelijk ook al
getest heeft). 
Dit hebben we even geparkeerd voor het vragenrondje van Nespresso. Volgende stap?
Nadenken over de homepage. Deze moet persoonlijker en de CTA moet veel 
duidelijker. Hoe kan ik dit gaan combineren? To be continued. 