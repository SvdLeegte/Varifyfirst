---
title: Creatief zit in de techniek
subtitle: cardsorting sessie - SPIE
date: 2019-09-12
tags: ["cardsorting", "SPIE", "creatievetechniek", "Empathy Map"]
---
Vandaag had ik de mogelijkheid om de voorbereidingen te treffen voor de 
cardsorting sessie met de klant, in dit geval SPIE. 
Claire heeft de voorbereiding gedaan betreft de powerpoint en wat er nog meer
nodig was naast de cards. 
Ik heb deze dag als super effectief en met een grote leercurve ervaren. 
Dan merk je pas dat een werknemer van het bedrijf waar je iets voor probeert
te verzinnen duidelijk moeilijk zichzelf in kan leven in de, in dit geval, 
potentiele klant. 
Deze sessie heb ik voornamelijk geobserveerd wat ik nodig heb om een creatieve
sessie te organiseren met de doelgroep. Ik had niet aan de empathy map gedacht, 
maar dit blijkt een erg goede toevoeging op een cardsorting sessie. 
Waarom? Omdat dit extra inzichten geeft van je gebruiker, en op deze manier kan 
je de uitkomsten van de cardsorting sessie meteen weerleggen met de gedachtes 
en pijnpunten van de gebruiker. 
Ik zou het fantastisch vinden om gedurende mijn stageperiode een soortgelijke
sessie te organiseren met ondersteuning van Claire. 
