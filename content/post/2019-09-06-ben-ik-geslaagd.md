---
title: Ben ik geslaagd
subtitle: als stagiaire?
date: 2019-09-06
tags: ["elastiekjes", "heelveelelastiekjes", "serietjekijken"]
---
### Ik heb deze week 16,5 uur gemaakt

Ja, dat kopje koffie haal ik ook voor mijn collega's maar dat doen ze ook heel
lief voor mij! ;) 
<br>
Vandaag heb ik lekker ontspannen zonder na te denken iets na mogen maken voor 
een van onze klanten. En soms moeten, ook als ze geen stagiaires hebben, 
andere simpele taken ook worden voldaan. Dus elastiekjes? **check**, kaartjes?
**check**, telefoon die opgeladen is zodat ik onder genot van een serie op 
netflix deze taak kan voltooien? **checkdubbelcheck**

# NEXT
<br>
<br> 
24,5/696
<br>
0/128