---
title: When your hobby 
subtitle: became your job
date: 2019-10-01
tags: ["hobby", "photography", "fotootjeschieten", "hoptjopgaserop"]
---
Oke, dit is een onderdeel dat in eerste instantie weinig tot geen koppeling 
heeft met mijn opleiding als CMD. Echter is dit wel een deel van mijn 
inkomsten gezien het onderdeel van mijn bedrijf is. Kleine miscommunicatie in 
de agenda van een collega zorgde ervoor dat ik snel even mijn camera mocht 
halen om wat foto's te schieten van het nieuwe pand van een van onze klanten. 
Niet iets waar ik normaal gesproken tijd insteek betreft fotografie, dus ook 
niet echt de richting waar ik de juiste lenzen voor tot mijn beschikking heb...
Gevolg? Vervormde foto's door de lens, en veel correctie nodig. De foto's gaan
tenslotte als mock-up gebruikt worden voor de ontwerper die gemaakt worden
door Weekend. 
Verder vandaag weinig nieuws op CMD gebied, enkel wat DTP werk en niet geheel
onbelangrijk: de spullen van ons huidige pand dienen verkocht te worden alvorens
we overgaan naar het nieuwe pand. 
<br>
<br>
moetookgebeuren ;)
