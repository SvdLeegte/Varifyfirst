---
title: Happy Newyear
subtitle: en een dagje voor Sanne
date: 2020-01-03
tags: ["eendagvrij", "meertijdvoormezelf", "rustig", "weinigtedoen"]
---

### Deze week heb ik 32,5 uur gewerkt

Oke, de vakantie vibe zit er bij de opdrachtgevers nog steeds in. Er ligt weinig
werk. In principe zouden Rowie en ik nog een creatieve sessie mogen leiden, 
maar ik vraag me af of we dat nog gaan halen alvorens de deadline van het 
stageverslag er is. Gelukkig hebben we samen al een creatieve sessie gehouden
waardoor ik mijn ideevorming gewoon volledig kan schrijven. Toch was het wel 
een leuke ervaring geweest om een sessie in mijn stageperiode te mogen leiden. 
<br>
<br> 
Deze week staat verder veel in het teken van YouTube filmpjes, literatuur,
maar ook zeker voor het stageverlag. Gelukkig heb ik een strakke planning, maar
ik merk dat ik geneigd ben er een beetje vanaf te wijken omdat het zo rustig
is op het kantoor. Daardoor moet ik nog een STARRT schrijven, terwijl mijn 
deadline was om het verslag vandaag tekstueel af te hebben. 
<br>
<br> 
A.d.h.v. YouTube filmpjes heb ik vandaag mijn 'must read' lijst aangevuld. Ik
moet nog wat geld gaan verdienen dit jaar om deze lijst te kunnen aanschaffen. 
;)
