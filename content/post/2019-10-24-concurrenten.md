---
Title: Concurrenten
subtitle: zijn er om van te leren
date: 2019-10-24
tags: ["concurrentenanalyse", "watkunnenwijgebruiken", "Saitaku", "zininsushi"]
---
Een concurrentenanalyse kun je op veel verschillende manieren inzetten. Ik 
heb hem dan ook op twee verschillende manieren ingestoken. Eentje op de 
producten van Saitaku zelf, wat bieden zij en wat bieden haar directe 
concurrenten daarin? En wat zijn de merkwaarden van Saitaku, en welke merken
binnen, bijvoorbeeld, de Albert Heijn zeggen hetzelfde te bieden? 
<br>
<br>
Verder bestond mijn dag uit interessante gesprekken op Marktplaats, en heb
ik als een fanatieke student gewerkt aan mijn stageplan deel II. 