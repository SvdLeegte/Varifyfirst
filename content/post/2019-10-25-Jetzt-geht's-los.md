---
title: Jetzt geht's los 
subtitle: over onproductieve vrijdagen gesproken
date: 2019-10-25
tags: ["onproductief", "oktoberfest", "niksgedaankrijgen", "concurrentenanalyse"]
---
### Deze week heb ik 40,25 uur gewerkt

Man, man, man. Het zal vast niet liggen aan het feit dat er vanavond een 
feestje op de planning staat. Ik merk dat het in de lucht hangt, want ik ben 
niet de enige die last heeft van concentratieproblemen. Mijn concentratieboog 
lijkt me ver vooruit te zijn en ik krijg hem amper tot niet te pakken. 
Ik heb nog even een poging gedaan om aan de concurrentenanalyse voor Saitaku
te werken. 
<br>
<br>
Oh wat misschien wel erg leuk is om te vertellen. Vanuit Dennis heb ik terug
gekregen dat Nespresso erg te spreken was over de keuzewijzer betreft de
koffies! Ze waren het ermee eens dat het niet duidelijk genoeg was als we 
kijken naar de verschillende soorten koffies die Nespresso te bieden heeft. 
Deze keuzewijzer kan hierbij oplossing bieden om verwarring te voorkomen. 
Positieve feedback vanuit de gebruiker & de opdrachtgever, hoe tof is dat?!
<br>
<br>
Nu is het tijd voor BIER! 
<br>
<br>
281.75/696
<br>
20/128