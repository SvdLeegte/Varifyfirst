---
Title: Drukte wedergekeerd
subtitle: in de DTP wereld
date: 2019-12-06
tags: ["correcties", "dtpleven", "ietsmetcentraleverwarming", "houopmetme"]
---

### Deze week heb ik 41,5 uur gewerkt. 

Oh god, we hebben een centrale verwarming... Op ons oude adres hadden we de 
luxe dat we met 6 koukleumen in een ruimte zaten en een persoonlijke 
verwarming. We hadden de temperatuur meestal dan ook op tropische temperaturen.
Nu niet meer... Dus dat wordt heeeeel veel laagjes, want het is KOUD! 
<br> 
<br> 
Deze week is DRUK, maar vooral met DTP correcties, en verder weinig CMD dingen.
Gewoon gas geven, en hier en daar tijd maken voor school. 
<br>
<br>
519,5/696
<br>
68/128