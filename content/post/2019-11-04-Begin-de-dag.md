---
title: Begin de dag
subtitle: met je mail openen
date: 2019-11-04
tags: ["marktplaats", "wireframes", "stageverslag", "vrijdagwasschooldag"]
---
De reden waarom vrijdag hier niet tussen staat is omdat ik de ochtend heb 
gebruikt voor het finetunen van mijn stageplan. Vervolgens stond de middag
gereserveerd voor een fijne terugkommiddag waar we de mogelijkheid hadden
om een start te maken aan een starrt. (: 
Vandaag dus weer even met marktplaats opstarten (weinig activiteit nu, maar 
sommige advertenties dienen opnieuw online gezet worden of er moet gekeken 
worden naar de vraagprijs), om vervolgens een vervolg te maken aan de 
wireframes waar ik inmiddels in Sketch mee aan de slag ben gegaan. Het begint
er beetje bij beetje op te lijken, maar ik krijg het vandaag nog niet helemaal
af. Aangezien Claire er morgen niet is, heb ik alvast een feedbackmoment in
haar agenda gezet voor woensdag. Ik heb het heel hard nodig, want er zijn nog 
heel veel dingen waar ik nog wel wat op moet aanpassen, enkel weet ik nu niet 
hoe ik dit aan moet pakken. 
Verder heb ik de laatste puntjes van mijn stageplan aangepast die ik in mijn 
laatste controle op vrijdag nog tegenkwam. Dit plan is opnieuw geupload in 
Natschool. 