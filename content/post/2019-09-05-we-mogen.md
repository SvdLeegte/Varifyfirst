---
title: We mogen
subtitle: van start 
date: 2019-09-05
tags: ["officieel", "fulltimer", "lachengierenbrullen"]
---

Vooruit met de geit. Na een paar dagen nieuwe eerstjaars op de vingers tikken
is het dan nu echt tijd om zelf op de vingers getikt te worden. 
En misschien op z'n tijd ook een compliment? Vandaag mocht ik meteen van start
om samen met Claire en Pascal de briefing over de Stoplichtapp van het Amphia
door te nemen. 
Hierin was het belangrijk dat de eenvoud van de app gehandhaafd werd, en de 
Amphia feeling, voor zover die erin zat, vervangen werd. 
Samen met Claire heb ik een schets gemaakt van een wireframe voor de app 
welke ik vervolgens in Sketch uitgewerkt heb als zijnde wireframe. 
<br>
<br>
Verder ben ik bezig geweest met de analyse van Bruynzeel Keukens. Deze analyse
kan worden gebruikt ter ondersteuning van de verbeterslag die doorgevoerd 
dient te worden vanuit Weekend voor Bruynzeel keukens.
<br>
<br>
Tot slot ben ik gestart met een debriefing te maken voor mezelf over de 
klant SPIE. Volgende week donderdag staat er een cardsorting sessie op de 
planning. Idealiter wordt deze uitgevoerd met de gebruiker, helaas zijn we 
door omstandigheden genoodzaakt om het met de stakeholder zelf uit te voeren. 
<br>
<br>
# Wordt vervolgd