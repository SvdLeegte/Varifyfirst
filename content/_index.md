## BLOG | Wat kan je hier vinden? 

Op deze blog zul je een wekelijkse, met uitzondering dagelijkse, update vinden 
van mijn werkzaamheden binnen WEEKEND creative agency in Breda. 
Ik ben hier vanaf begin september 2019 tot begin februari werkzaam als stagiair
UX Design. 
Deze blog zal dienen ter ondersteuning van mijn stageverslag welke als 
eindproduct opgeleverd gaat worden ter beoordeling van mijn functioneren 
als designer binnen WEEKEND creative agency. 
